﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Honan.wwwroot;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Honan
{
    public class PI_CertificateOfCurrencyModel : PageModel
    {
        private IWebHostEnvironment _hostingEnvironment;
        private IDataService dataService;
        public PI_CertificateOfCurrencyModel(IWebHostEnvironment hostingEnvironment, IDataService dataService)
        {
            this._hostingEnvironment = hostingEnvironment;
            this.dataService = dataService;
        }
        [BindProperty(SupportsGet = true)]
        public string stateAbbr { get; set; }
        public string clubName { get; set; }
        public SelectList States { get; set; }

        public void OnGet()
        {
            States = new SelectList(dataService.GetStates(), nameof(State.Abbreviation), nameof(State.Name));
        }
        public JsonResult OnGetClubs(string stateName)
        {
            return new JsonResult(dataService.GetClubs(stateName));
        }

        public async Task<IActionResult> OnPostDownloadAsync(string stateAbbr, string clubName)
        {
            if (clubName != null && clubName != "")
            {
                byte[] rawData = PDFGenerator.ReplaceText(_hostingEnvironment.WebRootPath + "\\downloads\\PersonalInjury\\HOCKEY AUSTRALIA LIMITED - Personal Accident COC.pdf", clubName);

                return File(rawData, "application/pdf");
            }
            else
            {
                return null;
            }
        }

    }
}