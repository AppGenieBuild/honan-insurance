﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Honan.wwwroot
{
    public static class PDFGenerator
    {
        public static byte[] ReplaceText(string pdfTemplate, string value)
        {
            MemoryStream outFile = new MemoryStream();
            using (outFile)
            {
                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, outFile);
                AcroFields fields = pdfStamper.AcroFields;
                //rest of the code here
                fields.SetField("CLUBNAME", value);
                //...
                pdfStamper.Close();
                pdfReader.Close();
            }
            return outFile.GetBuffer();
        }
    }
}
